package wei.ke.up.tfliteclassifier

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.google.android.material.snackbar.Snackbar
import wei.ke.up.tfliteclassifier.databinding.ActivityMainBinding
import wei.ke.up.tfliteclassifier.fragment.ClassifyFragment
import wei.ke.up.tfliteclassifier.fragment.SettingsFragment

class MainActivity : AppCompatActivity() {
    private val REQUEST_PERMISSIONS = 1
    private val settingsFragment = SettingsFragment()
    private val classifyFragment = ClassifyFragment()
    private val permissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
            .apply {
                supportFragmentManager.commit {
                    add(fragmentContainer.id, settingsFragment, "Settings")
                    add(fragmentContainer.id, classifyFragment, "Classify")
                    hide(settingsFragment)
                    show(classifyFragment)
                }
            }
        setContentView(binding.root)

        checkPermissions()
    }

    private fun checkPermissions() {
        if (!permissions.all {
                checkSelfPermission(it) == PackageManager.PERMISSION_GRANTED
            }) {
            requestPermissions(permissions, REQUEST_PERMISSIONS)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        grantResults.forEachIndexed { index, it ->
            if (it == PackageManager.PERMISSION_DENIED) {
                Snackbar.make(
                    binding.root,
                    "Need permission: ${permissions[index].split(".").last()}",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }
}