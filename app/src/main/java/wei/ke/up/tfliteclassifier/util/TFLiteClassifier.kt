package wei.ke.up.tfliteclassifier.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.RectF
import android.net.Uri
import android.util.Log
import wei.ke.up.tfliteclassifier.tflite.Detector
import wei.ke.up.tfliteclassifier.tflite.TFLiteObjectDetectionAPIModel
import java.io.FileInputStream
import java.nio.channels.FileChannel

object TFLiteClassifier {
    private val TAG = javaClass.simpleName
    private lateinit var detector: Detector
    var inputSize = 300
    var tfLiteModelPath = ""
    var tfLiteLabelmapPath = ""
    var minimumConfident = 0.5f

    fun init(context: Context): Boolean {
        if (tfLiteLabelmapPath.isEmpty() || tfLiteModelPath.isEmpty()) {
            return try {
                detector = TFLiteObjectDetectionAPIModel.create(
                    context,
                    "detect.tflite",
                    "labelmap.txt",
                    inputSize, true
                )
                true
            } catch (ex: Exception) {
                Log.e(TAG, "TFLite Classifier initialized fail!", ex)
                false
            }
        } else {
            val model = context.contentResolver
                .openFileDescriptor(Uri.parse(tfLiteModelPath), "r")?.let {
                    val ins = FileInputStream(it.fileDescriptor)
                    ins.channel
                        .map(FileChannel.MapMode.READ_ONLY, 0, it.statSize)
                } ?: return false

            val labels = context.contentResolver
                .openInputStream(Uri.parse(tfLiteLabelmapPath))
                ?.reader()
                ?.readLines()
                ?: return false

            return try {
                detector = TFLiteObjectDetectionAPIModel.create(model, labels, inputSize, false)
                true
            } catch (ex: Exception) {
                Log.e(
                    TAG,
                    "Load customize model without quantized failed! Try to load again with quantized."
                )
                try {
                    detector =
                        TFLiteObjectDetectionAPIModel.create(model, labels, inputSize, true)
                    true
                } catch (ex: Exception) {
                    Log.e(TAG, "Load customize model failed!")
                    false
                }
            }
        }
    }

    fun recognizeImage(
        bitmap: Bitmap,
        minimumConfident: Float = this.minimumConfident
    ): List<Detector.Recognition> =
        if (::detector.isInitialized) {

            try {
                detector.recognizeImage(
                    if (bitmap.width != inputSize || bitmap.height != inputSize)
                        Bitmap.createScaledBitmap(
                            bitmap,
                            inputSize,
                            inputSize,
                            false
                        )
                    else bitmap
                ).filter {
                    it.confidence >= minimumConfident
                }.map {
                    val wRatio = bitmap.width * 1.0f / inputSize
                    val hRatio = bitmap.height * 1.0f / inputSize
                    val rect = it.location
                    it.location = RectF(
                        rect.left * wRatio,
                        rect.top * hRatio,
                        rect.right * wRatio,
                        rect.bottom * hRatio
                    )
                    it
                }
            } catch (ex: Exception) {
                listOf()
            }
        } else listOf()
}
