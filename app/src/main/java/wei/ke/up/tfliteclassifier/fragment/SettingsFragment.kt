package wei.ke.up.tfliteclassifier.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import wei.ke.up.tfliteclassifier.R
import wei.ke.up.tfliteclassifier.databinding.FragmentSettingsBinding
import wei.ke.up.tfliteclassifier.util.TFLiteClassifier

class SettingsFragment : Fragment(R.layout.fragment_settings) {
    private val TAG = javaClass.simpleName
    private val REQUEST_SELECT_TFLITE_MODEL = 1
    private val REQUEST_SELECT_LABELMAP = 2
    private var binding: FragmentSettingsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(inflater)
            .apply {
                selectTFLiteModelBtn.setOnClickListener {
                    selectFile(REQUEST_SELECT_TFLITE_MODEL, "application/octet-stream")
                }

                selectLabelmapFileBtn.setOnClickListener {
                    selectFile(REQUEST_SELECT_LABELMAP, "text/plain")
                }

                minConfidenceValue.addTextChangedListener {
                    TFLiteClassifier.minimumConfident =
                        minConfidenceValue.text.toString().toFloatOrNull() ?: 0f
                }

                inputSizeValue.addTextChangedListener {
                    TFLiteClassifier.inputSize = inputSizeValue.text.toString().toIntOrNull() ?: 300
                }
            }
        return binding?.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_SELECT_TFLITE_MODEL -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        TFLiteClassifier.tfLiteModelPath = it.toString()
                        binding?.tflitePathText?.text = getFileName(it)
                    }
                }
            }
            REQUEST_SELECT_LABELMAP -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        TFLiteClassifier.tfLiteLabelmapPath = it.toString()
                        binding?.labelmapPathText?.text = getFileName(it)
                    }
                }
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (hidden) {
            TFLiteClassifier.init(requireContext())
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    private fun selectFile(requestCode: Int, type: String) {

        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            this.type = type
        }
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            startActivityForResult(intent, requestCode)
        }
    }

    private fun getFileName(it: Uri): CharSequence? {
        val cursor = requireContext().contentResolver?.query(
            it,
            arrayOf(OpenableColumns.DISPLAY_NAME),
            null,
            null,
            null
        )
        return cursor?.let {
            it.moveToFirst()
            val result = it.getString(0)
            it.close()
            result
        }
    }
}