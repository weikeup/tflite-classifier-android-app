package wei.ke.up.tfliteclassifier.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.graphics.applyCanvas
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import wei.ke.up.tfliteclassifier.R
import wei.ke.up.tfliteclassifier.adapter.TFLiteRecognizeResultListAdapter
import wei.ke.up.tfliteclassifier.databinding.FragmentClassifyBinding
import wei.ke.up.tfliteclassifier.tflite.Detector
import wei.ke.up.tfliteclassifier.util.TFLiteClassifier
import wei.ke.up.tfliteclassifier.util.YuvToRgbConverter
import java.io.FileNotFoundException
import java.util.*
import kotlin.math.max

class ClassifyFragment : Fragment() {
    private val TAG = javaClass.simpleName
    private val REQUEST_PERMISSION = 1
    private val REQUEST_SELECT_IMAGE = 2
    private val yuvToRgbConverter by lazy { YuvToRgbConverter(requireContext()) }
    private val recognizeResult = MutableLiveData<List<Detector.Recognition>>()
    private val isCameraOn = MutableLiveData(false)
    private val colors by lazy {
        val array = arrayListOf<Int>()
        for (i in 0 until 20) {
            array.add(
                Color.HSVToColor(
                    floatArrayOf(
                        i % 10 * 40f,
                        1f,
                        1f
                    )
                )
            )
        }
        array
    }
    private var binding: FragmentClassifyBinding? = null
    private lateinit var cameraProvider: ProcessCameraProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        TFLiteClassifier.init(requireContext())

        ProcessCameraProvider.getInstance(requireContext())
            .apply {
                addListener(
                    {
                        cameraProvider = get()
                    },
                    ContextCompat.getMainExecutor(requireContext())
                )
            }

        binding = FragmentClassifyBinding.inflate(inflater)
            .apply {
                resultList.layoutManager = LinearLayoutManager(requireContext())
                recognizeResult.observe(viewLifecycleOwner, {
                    resultList.adapter = TFLiteRecognizeResultListAdapter(
                        colors,
                        it
                    )
                })

                isCameraOn.observe(viewLifecycleOwner, {
                    if (it) {
//                        imageView.visibility = View.INVISIBLE
                        cameraView.keepScreenOn = true
                        togglePreviewFAB.backgroundTintList =
                            ColorStateList.valueOf(
                                resources.getColor(
                                    R.color.togglePreviewFAB_off,
                                    null
                                )
                            )
                    } else {
                        cameraView.keepScreenOn = false
                        togglePreviewFAB.backgroundTintList =
                            ColorStateList.valueOf(
                                resources.getColor(
                                    R.color.togglePreviewFAB_on,
                                    null
                                )
                            )

                    }
                })

                takePhotoFAB.setOnClickListener {
//                    if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
//                    ) {
//                        requireActivity().requestPermissions(
//                            arrayOf(Manifest.permission.CAMERA),
//                            REQUEST_PERMISSION
//                        )
//                        return@setOnClickListener
//                    }

                    val imageCapture = ImageCapture.Builder()
                        .build()
                    try {
                        cameraProvider.bindToLifecycle(
                            this@ClassifyFragment,
                            CameraSelector.DEFAULT_BACK_CAMERA,
                            imageCapture
                        )
                        imageCapture.takePicture(
                            ContextCompat.getMainExecutor(requireContext()),
                            object : ImageCapture.OnImageCapturedCallback() {
                                @SuppressLint("UnsafeExperimentalUsageError")
                                override fun onCaptureSuccess(image: ImageProxy) {
                                    val bytes = ByteArray(image.planes[0].buffer.capacity())
                                    image.planes[0].buffer.get(bytes)
                                    var bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                                    val matrix = Matrix()
                                        .apply {
                                            setRotate(image.imageInfo.rotationDegrees.toFloat())
                                        }
                                    bitmap = Bitmap.createBitmap(
                                        bitmap,
                                        0,
                                        0,
                                        bitmap.width,
                                        bitmap.height,
                                        matrix,
                                        false
                                    )

                                    val results = TFLiteClassifier.recognizeImage(
                                        bitmap
                                    )
                                    bitmap = bitmap.drawResultRect(results)
                                    recognizeResult.value = results
                                    imageView.setImageBitmap(bitmap)
//                                    imageView.visibility = View.VISIBLE
                                    cameraProvider.unbindAll()
                                    isCameraOn.value = false
                                    image.close()
                                }

                                override fun onError(exception: ImageCaptureException) {
                                    Log.e(
                                        TAG,
                                        "Image capture error!",
                                        exception
                                    )
                                    cameraProvider.unbind(imageCapture)
                                }
                            }
                        )
                    } catch (ex: Exception) {
                        Log.e(TAG, "Failed to take a photo!", ex)
                        cameraProvider.unbind(imageCapture)
                    }
                }

                togglePreviewFAB.setOnClickListener {
//                    if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
//                    ) {
//                        requireActivity().requestPermissions(
//                            arrayOf(Manifest.permission.CAMERA),
//                            REQUEST_PERMISSION
//                        )
//                        return@setOnClickListener
//                    }

                    if (isCameraOn.value == true) {
                        cameraProvider.unbindAll()
                        isCameraOn.value = false
                    } else {
                        startCameraPreview(cameraProvider)
                        isCameraOn.value = true
                    }
                }

                loadImageFAB.setOnClickListener {
//                    if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
//                    ) {
//                        requireActivity().requestPermissions(
//                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
//                            REQUEST_PERMISSION
//                        )
//                        return@setOnClickListener
//                    }

                    val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                        type = "image/*"
                    }
                    if (intent.resolveActivity(requireContext().packageManager) != null) {
                        isCameraOn.value = false
                        cameraProvider.unbindAll()
                        startActivityForResult(intent, REQUEST_SELECT_IMAGE)
                    }
                }

                settingsBtn.setOnClickListener {
                    isCameraOn.value = false
                    cameraProvider.unbindAll()
                    parentFragmentManager.run {
                        commit {
                            hide(this@ClassifyFragment)
                            show(findFragmentByTag("Settings")!!)
                            addToBackStack(null)
                        }
                    }
                }
            }
        return binding?.root
    }

    private fun Bitmap.drawResultRect(
        results: List<Detector.Recognition>
    ): Bitmap {
        val paint = Paint().apply {
            style = Paint.Style.STROKE
            strokeWidth = 0.005f * max(this@drawResultRect.width, this@drawResultRect.height)
        }
        return this.copy(Bitmap.Config.ARGB_8888, true).applyCanvas {
            results.forEachIndexed { index, recognition ->
                paint.color =
                    if (index < colors.size) colors[index]
                    else Color.RED
                drawRect(recognition.location, paint)
            }
        }
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    private fun startCameraPreview(cameraProvider: ProcessCameraProvider) {
//        val preview = Preview.Builder()
//            .build()
//            .apply {
//                setSurfaceProvider(binding?.cameraView?.surfaceProvider)
//            }

        val imageAnalysis = ImageAnalysis.Builder()
            .build()
            .apply {
                setAnalyzer(
                    ContextCompat.getMainExecutor(requireContext()),
                    { image ->
                        image.image?.let {
                            Log.d(TAG, "Analysis image size: ${it.width} x ${it.height}")
                            var bitmap =
                                Bitmap.createBitmap(it.width, it.height, Bitmap.Config.ARGB_8888)
                            yuvToRgbConverter.yuvToRgb(it, bitmap)
                            val matrix = Matrix()
                                .apply {
                                    setRotate(image.imageInfo.rotationDegrees.toFloat())
                                }
                            bitmap = Bitmap.createBitmap(
                                bitmap,
                                0,
                                0,
                                bitmap.width,
                                bitmap.height,
                                matrix,
                                false
                            )

                            val results = TFLiteClassifier.recognizeImage(
                                bitmap
                            )
                            bitmap = bitmap.drawResultRect(results)
                            binding?.imageView?.setImageBitmap(bitmap)
                            recognizeResult.value = results
                        }
                        image.close()
                    }
                )
            }

        cameraProvider.unbindAll()
        try {
            cameraProvider.bindToLifecycle(
                this,
                CameraSelector.DEFAULT_BACK_CAMERA,
                //preview,
                imageAnalysis
            )
        } catch (ex: Exception) {
            Log.e(TAG, "CameraProvider binding failed!", ex)
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
//        if (permissions.contains(Manifest.permission.CAMERA)) {
//            if (grantResults[permissions.indexOf(Manifest.permission.CAMERA)] == PackageManager.PERMISSION_DENIED) {
//                Snackbar.make(
//                    requireView(),
//                    "Need camera permission!",
//                    Snackbar.LENGTH_LONG
//                ).show()
//            }
//        }
//        if (permissions.contains(Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            if (grantResults[permissions.indexOf(Manifest.permission.READ_EXTERNAL_STORAGE)] == PackageManager.PERMISSION_DENIED) {
//                Snackbar.make(
//                    requireView(),
//                    "Need read external storage permission!",
//                    Snackbar.LENGTH_LONG
//                ).show()
//            }
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_SELECT_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        try {
                            requireContext().contentResolver
                                .openFileDescriptor(it, "r")
                                ?.let { fd ->
                                    var bitmap =
                                        BitmapFactory.decodeFileDescriptor(fd.fileDescriptor)
                                    val results = TFLiteClassifier.recognizeImage(bitmap)
                                    bitmap = bitmap.drawResultRect(results)
                                    binding?.imageView?.setImageBitmap(bitmap)
                                    recognizeResult.value = results
                                    fd.close()
                                }
                        } catch (fex: FileNotFoundException) {
                            Snackbar.make(requireView(), "File not found!", Snackbar.LENGTH_LONG)
                                .show()
                        }
                    }
                }
            }
        }
    }
}
