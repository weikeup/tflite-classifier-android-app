package wei.ke.up.tfliteclassifier.adapter


import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import wei.ke.up.tfliteclassifier.databinding.LayoutClassifyResultListBinding
import wei.ke.up.tfliteclassifier.tflite.Detector

class TFLiteRecognizeResultListAdapter(
    private val colors: List<Int>,
    private val results: List<Detector.Recognition>
) :
    RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        LayoutClassifyResultListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            .run {
                return ViewHolder(this, root)
            }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = results[position].title
        holder.score.text = String.format("%.04f", results[position].confidence)
        holder.setBackgroundColor(if (position < colors.size) colors[position] else Color.RED)
    }

}

class ViewHolder(private val binding: LayoutClassifyResultListBinding, itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    val name = binding.name
    val score = binding.score

    fun setBackgroundColor(color: Int) {
        binding.root.setBackgroundColor(color)
    }
}